"use client";

import React, { useState, useEffect } from "react";
import Image from "next/image"
import Link from "next/link";
import avatar from "../../images/image-avatar.png"
import cart from "../../images/icon-cart.svg"
import remove from '../../images/icon-delete.svg'
import close from "../../images/icon-close.svg"
import menu from "../../images/icon-menu.svg"

interface CartItem {
  thumbnail: string;
  name: string;
  price: string;
  quantity: number;
}

const Navbar = () => {
  const [navMobile, setNabvMobile] = useState(false);

  const [cartItems, setCartItems] = useState<CartItem[]>([]);
  const handleNabvMobile = () => {
    setNabvMobile(!navMobile);
  };

  const [openCart, setopenCart] = useState(false);
  const handleClose = () => {
    setopenCart(!openCart);
  };

  useEffect(() => {
    const storedCart = localStorage.getItem("cart");
    const initialCart: CartItem[] = storedCart ? JSON.parse(storedCart) : [];
    setCartItems(initialCart);
  }, []);

  const handleRemoveFromCart = (index: number) => {
    const updatedCart = [...cartItems];
    updatedCart.splice(index, 1);
    setCartItems(updatedCart);

    // Update localStorage
    localStorage.setItem("cart", JSON.stringify(updatedCart));
  };

  return (
    <>
      <div className="Navbar bg-white lg:px-[50px] px-5 z-10">
        <div className="flex justify-between items-center w-auto border-b-[1.5px] border-b-stone-300">
          <div className="flex lg:justify-center justify-start items-center gap-5 h-20">
            <div
              className="nav-mobile lg:hidden block"
              onClick={handleNabvMobile}
            >
              <Image src={menu} alt="icon menu" />
            </div>
            <Link href="/" className="text-3xl font-bold">
              sneakers
            </Link>
            <div className="gap-5 h-full items-center hidden lg:flex w-full">
              <a className="text-[15px] text-Grayish-blue">Collections</a>
              <a className="text-[15px] text-Grayish-blue">Men</a>
              <a className="text-[15px] text-Grayish-blue">Women</a>
              <a className="text-[15px] text-Grayish-blue">About</a>
              <a className="text-[15px] text-Grayish-blue">Contact</a>
            </div>
          </div>
          <div className="flex gap-4 lg:gap-10 items-center">
            <div className="cart" onClick={handleClose}>
              <Image src={cart} alt="iconCart"></Image>
            </div>
            <div className="profile w-11 border-2 border-orange-500 rounded-full">
              <Image src={avatar} alt="AvatarProfile" />
            </div>
          </div>
        </div>

        {/* cart modal */}
        {openCart && (
          <div className="w-[350px] h-auto bg-white rounded-lg flex flex-col gap-4 z-10 text-lg p-4 shadow box-border absolute ml-[600px] mt-[160px]">
            <div className="flex flex-row w-full justify-between">
            <div className="text-left">Cart</div>
            <Image src={close} alt="close" className="w-[20px] h-[20px]" onClick={handleClose}/>
            </div>
            <div >
              {cartItems.map((item, index) => (
                <div key={index}>
                  <div className="flex flex-row items-center gap-x-8">
                  <Image src={item.thumbnail} alt="product" className="w-[50px] h-[50]"/>
                  <div className="flex flex-col">
                    <div className="text-left">{item.name}</div>
                    <div className="flex flex-row gap-x-4">
                        <div>${item.price}</div>
                        <div>
                          x {item.quantity}
                        </div>
                        <div>
                        ${parseFloat(item.price) * item.quantity}
                        </div>
                    </div>
                  </div>
                  <div>
                  <Image
                    src={remove}
                    className="w-[17px] h-[17px]"
                    alt="remove Cart"
                    onClick={() => {
                      handleRemoveFromCart(index);
                    }}
                    />
                  </div>
                  </div>
                </div>
              ))}
            </div>
            <button className="bg-Orange  h-[40px] rounded-lg">
                <div>Checkout</div>
            </button>
          </div>
        )}

        {/* mobile navbar */}
        {navMobile === true && (
        <div
          className={`z-30 bg-white w-3/4 h-[150vh] absolute top-0 lg:hidden block ${
            true === true ? "left-0" : "-left-96"
          }`}
        >
          <div className="flex flex-col gap-4 p-3">
            <Image src={close} alt="iconMenu" onClick={handleNabvMobile} />
            <div className="flex flex-col">
              <p>Collections</p>
              <p>Men</p>
              <p>Woman</p>
              <p>About</p>
              <p>Contact</p>
            </div>
          </div>
        </div>
      )}
    </div>
    </>
  )
}

export default Navbar
