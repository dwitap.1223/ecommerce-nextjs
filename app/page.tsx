"use client";

import Image from 'next/image'
import product1 from "./images/image-product-1.jpg"
import product2 from "./images/image-product-2.jpg"
import product3 from "./images/image-product-3.jpg"
import product4 from "./images/image-product-4.jpg"
import cartImg from "./images/icon-cart.svg"
import minus from "./images/icon-minus.svg"
import plus from "./images/icon-plus.svg"
import { useEffect, useState } from 'react'

export default function Home() {
  const [quantity, setQuantity] = useState<number>(0);

  const imageAssets = [
    product1,
    product2,
    product3,
    product4
  ];

  const [selectedImage, setSelectedImage] = useState(imageAssets[0]);
  const [cart, setCart] = useState<any>([]);

  const handleCounter = (action: string): void => {
    if (action === "plus") {
      setQuantity(quantity + 1);
    } else {
      if (quantity < 1) return;
      setQuantity(quantity - 1);
    }
  };

  const handleThumbnailClick = (newImage: any) => {
    setSelectedImage(newImage);
  };

  const addToCart = () => {
    const result = {
      thumbnail: selectedImage,
      name: "Fall Limited Edition Sneakers",
      price: "125.00",
      quantity: quantity,
    };
    setCart([...cart, result]);
    setQuantity(0);
  };

  // data tidak hilang saat refresh
  useEffect(() => {
    const storedCart = localStorage.getItem("cart");
    const initialCart = storedCart ? JSON.parse(storedCart) : [];
    setCart(initialCart);
  }, []);

  useEffect(() => {
    localStorage.setItem("cart", JSON.stringify(cart));
  }, [cart]);

  return (
    <div className='flex w-screen mx-8 box-border items-center'>
    <div className='flex flex-col lg:flex-row w-[80vw] mt-[50px] gap-[50px]'>
      <div className='flex items-center flex-col gap-5 lg:w-[717px]'>
        <Image src={selectedImage} alt='' className='w-[445px] h-[445px] rounded-2xl'/>
        <div className='grid grid-cols-4 w-[447px] gap-5'>
        {imageAssets.map((image, index) => (
              <div
                key={index}
                onClick={() => handleThumbnailClick(image)}
              >
                <Image src={image} alt='product' className='w-[92px] h-[92px] rounded-xl'/>
              </div>
            ))}
        </div>
      </div>
      <div className='flex flex-col lg:w-[550px] w-screen gap-[30px]'>
        <div className='text-Orange text-sm font-bold tracking-[1.12px]'>Sneaker Company</div>
        <div className='text-Very-dark-blue text-[44px] font-bold leading-[48px]'>Fall Limited Edition  Sneakers</div>
        <div className='text-Grayish-blue text-base font-medium leading-[26px]'>These low-profile sneakers are your perfect casual wear  companion. Featuring a durable rubber outer sole, they’ll withstand everything the weather can offer. </div>
        <div className='grid grid-cols-2 w-[272px] justify-center'>
          <div className='text-Very-dark-blue text-3xl font-bold leading-[26px]'>$125.00
          </div>
          <div className='w-[52px] h-[27px] text-Orange text-base font-bold leading-[26px] bg-Pale-orange text-center rounded-[5px]'>
            50%
          </div>
        </div>
        <div className='text-Very-dark-blue text-base font-bold leading-[26px] tracking-[0.48px] line-through'>$250.00</div>
        <div className='flex flex-row gap-4'>
          <div className='flex flex-row' >
            <button className='bg-Light-grayish-blue
            rounded-[10px] w-[50px] flex justify-center items-center' onClick={() => handleCounter("minus")}><Image src={minus} alt="minus"/></button>
            <div className='bg-Light-grayish-blue
            rounded-[10px] w-[57px] flex justify-center items-center'>{quantity}</div> 
            <button className='bg-Light-grayish-blue
            rounded-[10px] w-[50px] flex justify-center items-center' onClick={() => handleCounter("plus")}><Image src={plus} alt="plus"/></button>
          </div>
          <button className='bg-Orange text-white text-base font-bold lg:w-[273px] w-full h-[54px] rounded-[10px] flex justify-center items-center gap-4'  onClick={addToCart}>
            <Image src={cartImg} alt="cart"/>
          Add to cart
          </button>
        </div>
      </div>
    </div>
    </div>
  )
}

